﻿using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;

namespace MotivationGenerator
{
    /// <summary>
    /// Класс добавления к основной картинке цветного фона.
    /// </summary>
    public class DrawingFrame : BaseImageGen
    {
        public DrawingFrame(IImageGener imgGen) 
            : base(imgGen)
        {
        }

        public override Image Execute()
        {
            var img = base.Execute();

            // Создаем прямоугольник для закрашивания основной картинки.
            var rect = new Rectangle(0, 0, width: img.Width, height: img.Height);

            // Добавляем закрашенный прямоугольник к основной картинке.
            img.Mutate(x => x.BackgroundColor(Color.FromRgb(74, 135, 216), rect));

            return img;
        }
    }
}
