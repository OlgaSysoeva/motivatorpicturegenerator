﻿using SixLabors.ImageSharp;

namespace MotivationGenerator
{
    /// <summary>
    /// Класс для сохранения готовой картинки-мотиватора.
    /// </summary>
    public class SaveImg : BaseImageGen
    {
        /// <summary>
        /// Путь сохранения картинки-мотиватора.
        /// </summary>
        private string _motivPath;

        public SaveImg(IImageGener imgGen, string motivPath)
            : base(imgGen)
        {
            _motivPath = motivPath;
        }

        public override Image Execute()
        {
            var img = base.Execute();

            img.SaveAsJpeg(_motivPath);

            return img;
        }
    }
}
