﻿using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;

namespace MotivationGenerator
{
    /// <summary>
    /// Класс для создания основы картинки-мотиватора.
    /// </summary>
    public class ImageGener : IImageGener
    {
        /// <summary>
        /// Ширина основной картинки.
        /// </summary>
        private int _width;

        /// <summary>
        /// Высота основной картинки.
        /// </summary>
        private int _height;

        public ImageGener(string img, int padding, int textHeight)
        {
            var image = Image.Load(img);

            _width = image.Width + 2 * padding;
            _height = image.Height + 3 * padding + textHeight;
        }

        public virtual Image Execute()
        {
             return new Image<Rgba32>(_width, _height);
        }
    }
}
