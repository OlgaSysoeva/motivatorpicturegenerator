﻿using System;

namespace MotivationGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true)
            {
                Console.Write("Укажите картинку: ");
                var img = Console.ReadLine();

                Console.Write("Введите текст: ");
                var txt = Console.ReadLine();

                var mg = new Generator();
                var creatFile = mg.CreateMotivator(img, txt);

                Console.WriteLine($"Файл создан: {creatFile}");
            }
        }
    }
}
