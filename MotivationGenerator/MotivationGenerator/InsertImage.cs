﻿using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;

namespace MotivationGenerator
{
    /// <summary>
    /// Класс для вставки в основное изобравжение картинки-мотиватора.
    /// </summary>
    public class InsertImage : BaseImageGen
    {
        /// <summary>
        /// Путь к добавляемой картинке.
        /// </summary>
        private string _imgPath;

        /// <summary>
        /// Размер отступа от края до картинки-мотиватора.
        /// </summary>
        private int _padding;

        public InsertImage(
            IImageGener imgGen, 
            string imgPath,
            int padding)
            : base(imgGen)
        {
            _imgPath = imgPath;
            _padding = padding;
        }

        public override Image Execute()
        {
            var img = base.Execute();

            // Загружаем картинку-мотиватор для вставки.
            var imageInsert = Image.Load(_imgPath);

            // Добавляем к исходной картинке изображение.
            img.Mutate(x => x.DrawImage(imageInsert, new Point(_padding, _padding), 1));

            return img;
        }
    }
}
