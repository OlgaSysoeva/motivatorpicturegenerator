﻿using System;
using System.IO;

namespace MotivationGenerator
{
    /// <summary>
    /// Фасадный класс-генератор картинок-мотиваторов.
    /// Используются паттерны: фасад, декоратор.
    /// </summary>
    public class Generator
    {
        /// <summary>
        /// Путь сохранения картинки с указанием имени и расширения.
        /// </summary>
        private string _motivPath = Path.Combine(
            AppDomain.CurrentDomain.BaseDirectory, 
            $"motivator{(new Random()).Next(99)}.jpg");

        /// <summary>
        /// Отступ от края рамки до картинки.
        /// </summary>
        const int Padding = 20;

        /// <summary>
        /// Высота блока, в котором расположен текст.
        /// </summary>
        const int TextHeight = 100;

        /// <summary>
        /// Размер шрифта текста.
        /// </summary>
        const int FontSize = 30;

        /// <summary>
        /// Метод создания картинки-мотиватора.
        /// </summary>
        /// <param name="img">Путь к картинке.</param>
        /// <param name="txt">Текст-мотиватор.</param>
        /// <returns>Возвращает путь к созданному мотиватору.</returns>
        public string CreateMotivator(string img, string txt)
        {
            // Создание основного объекта-картинки.
            IImageGener imgGen = new ImageGener(img, Padding, TextHeight);

            // Добавление рамки.
            imgGen = new DrawingFrame(imgGen);

            // Вставка картинки.
            imgGen = new InsertImage(imgGen, img, Padding);

            // Вставка текста
            imgGen = new InsertText(imgGen, txt, Padding, TextHeight, FontSize);

            // Сохранение в файл
            imgGen = new SaveImg(imgGen, _motivPath);

            // Запуск процесса генерации мотиватора.
            imgGen.Execute();

            return _motivPath;
        }
    }
}
