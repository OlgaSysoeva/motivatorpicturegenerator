﻿using SixLabors.ImageSharp;

namespace MotivationGenerator
{
    /// <summary>
    /// Базовый класс Декоратора.
    /// Определяет интерфейс для всех дочерних классов-декораторов.
    /// </summary>
    public class BaseImageGen : IImageGener
    {
        protected IImageGener _imgGen;

        public BaseImageGen(IImageGener imgGen)
        {
            _imgGen = imgGen;
        }

        public virtual Image Execute()
        {
            return _imgGen.Execute();
        }
    }
}
