﻿using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Drawing.Processing;
using SixLabors.ImageSharp.Processing;

namespace MotivationGenerator
{
    /// <summary>
    /// Класс для добавления текста к исходной картинке.
    /// </summary>
    public class InsertText : BaseImageGen
    {
        /// <summary>
        /// Текст для вставки.
        /// </summary>
        readonly string _text;

        /// <summary>
        /// Размер отступа от края до картинки-мотиватора.
        /// </summary>
        readonly int _padding;

        /// <summary>
        /// Высота блока, в котором расположен текст.
        /// </summary>
        readonly int _textHeight;

        /// <summary>
        /// Размер шрифта текста.
        /// </summary>
        readonly int _fontSize;

        public InsertText(
            IImageGener imgGen, 
            string text,
            int padding,
            int textHeight,
            int fontSize)
            : base(imgGen)
        {
            _text = text;
            _padding = padding;
            _textHeight = textHeight;
            _fontSize = fontSize;
        }

        public override Image Execute()
        {
            var img = base.Execute();

            // Создание шрифта текста.
            Font font = SystemFonts.CreateFont("Arial", _fontSize);

            // Задание координат расположения текста.
            var point = new PointF(_padding, img.Height - (_textHeight-_padding));

            // Задание параметров для форматирования текста.
            var textGraphicsOptions = new TextGraphicsOptions()
            {
                TextOptions = {
                        WrapTextWidth = img.Width - 2 * _padding,
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Center
                }
            };

            // Добавление к исходной картинке текста.
            img.Mutate(x => x.DrawText(textGraphicsOptions, _text, font, Color.White, point));

            return img;
        }
    }
}
