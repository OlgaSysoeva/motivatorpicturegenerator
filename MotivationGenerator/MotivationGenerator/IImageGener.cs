﻿using SixLabors.ImageSharp;

namespace MotivationGenerator
{
    /// <summary>
    /// Интерфейс запуска генерации.
    /// </summary>
    public interface IImageGener
    {
        /// <summary>
        /// Метод запуска генерации мотиватора.
        /// </summary>
        /// <returns>Возвращает изображение.</returns>
        Image Execute();
    }
}
